package com.amastudio.pull_up.Activityes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.amastudio.pull_up.R;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {

    Button trainingBtn, targetBtn, statisticsBtn, leadersBtn,
            alarmBtn, settingsBtn, developersBtn, moreBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        trainingBtn = findViewById(R.id.training_dashboard_btn);
        trainingBtn.setOnClickListener(this);

        targetBtn = findViewById(R.id.target_dashboard_btn);
        targetBtn.setOnClickListener(this);

        statisticsBtn = findViewById(R.id.statistics_dashboard_btn);
        statisticsBtn.setOnClickListener(this);

        leadersBtn = findViewById(R.id.leaders_dashboard_btn);
        leadersBtn.setOnClickListener(this);

        alarmBtn = findViewById(R.id.alarm_dashboard_btn);
        alarmBtn.setOnClickListener(this);

        settingsBtn = findViewById(R.id.settings_dashboard_btn);
        settingsBtn.setOnClickListener(this);

        developersBtn = findViewById(R.id.developers_dashboard_btn);
        developersBtn.setOnClickListener(this);

        moreBtn = findViewById(R.id.more_app_dashboard_btn);
        moreBtn.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.training_dashboard_btn:
                Intent intent1 = new Intent(this, TrainingActivity.class);
                startActivity(intent1);
                break;
            case R.id.target_dashboard_btn:
                Intent intent2 = new Intent(this, TargetActivity.class);
                startActivity(intent2);
                break;
            case R.id.statistics_dashboard_btn:
                Intent intent3 = new Intent(this, StatisticsActivity.class);
                startActivity(intent3);
                break;
            case R.id.leaders_dashboard_btn:
                Intent intent4 = new Intent(this, LeadersActivity.class);
                startActivity(intent4);
                break;
            case R.id.alarm_dashboard_btn:
                Intent intent5 = new Intent(this, AlarmActivity.class);
                startActivity(intent5);
                break;
            case R.id.settings_dashboard_btn:
                Intent intent6 = new Intent(this, SettingsActivity.class);
                startActivity(intent6);
                break;
            case R.id.developers_dashboard_btn:
                Intent intent7 = new Intent(this, DevelopersActivity.class);
                startActivity(intent7);
                break;
            case R.id.more_app_dashboard_btn:
                Intent intent8 = new Intent(this, MoreAppActivity.class);
                startActivity(intent8);
                break;
        }
    }
}
