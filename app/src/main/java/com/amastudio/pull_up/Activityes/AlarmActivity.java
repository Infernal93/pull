package com.amastudio.pull_up.Activityes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.DialogFragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TimePicker;

import com.amastudio.pull_up.Adapters.AlarmBootReceiver;
import com.amastudio.pull_up.Adapters.AlertReceiver;
import com.amastudio.pull_up.Fragments.TimePickerFragment;
import com.amastudio.pull_up.R;

import java.text.DateFormat;
import java.util.Calendar;


public class AlarmActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {


    private EditText mondayText;
    public SwitchCompat mondaySwitch;
    SharedPreferences sPref;
    String timeText = "";
    final String SAVED_TEXT = "saved_text";
    boolean switch_On_Off;
    public static final String PREFS_NAME = "Switch_On_Off_check";
    final Calendar c = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        mondayText = findViewById(R.id.monday_time);
        mondaySwitch = findViewById(R.id.switch_monday);
        // load SharedPref save text in mondayText and save switch On else Off
        loadText();
        loadSwitchCheck();

        mondaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mondaySwitch.isChecked()) {
                    startAlarm(c);
                    bootStartAlarm(c);
                }
                saveSwitchCheck();
            }
        });

        mondayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        updateTimeText(c);
        saveText();
        mondaySwitch.setChecked(false);
        bootStopAlarm(c);
    }

    private void updateTimeText(Calendar c) {
        timeText = "";
        timeText += DateFormat.getTimeInstance(DateFormat.SHORT).format(c.getTime());
        mondayText.setText(timeText);
    }

    public void startAlarm(Calendar c) {

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlertReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        if (c.before(Calendar.getInstance())) {
            c.add(Calendar.DATE, 1);
        }
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

    public void bootStartAlarm(Calendar c) {

        PackageManager packageManager = AlarmActivity.this.getPackageManager();
        ComponentName componentName = new ComponentName(AlarmActivity.this, AlarmBootReceiver.class);
        packageManager.setComponentEnabledSetting(componentName,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmBootReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

    public void bootStopAlarm(Calendar c) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmBootReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
        PackageManager packageManager = AlarmActivity.this.getPackageManager();
        ComponentName componentName = new ComponentName(AlarmActivity.this, AlarmBootReceiver.class);
        packageManager.setComponentEnabledSetting(componentName,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void saveText() {
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(SAVED_TEXT, timeText);
        ed.commit();
        //Toast.makeText(AlarmActivity.this, "Text saved", Toast.LENGTH_SHORT).show();
    }

    private void loadText() {

        sPref = getPreferences(MODE_PRIVATE);
        String savedText = sPref.getString(SAVED_TEXT, "");
        mondayText.setText(savedText);
        // Toast.makeText(AlarmActivity.this, "Text loaded", Toast.LENGTH_SHORT).show();
    }

    private void saveSwitchCheck() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("switchKey", mondaySwitch.isChecked());
        editor.commit();
    }

    private void loadSwitchCheck() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        switch_On_Off = settings.getBoolean("switchKey", false);
        mondaySwitch.setChecked(switch_On_Off);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadText();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveText();
        saveSwitchCheck();

    }


    //   private void cancelAlarm() {
    //    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    //    Intent intent = new Intent(this, AlertReceiver.class);
    //    PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
    //    alarmManager.cancel(pendingIntent);
    //    mondayText.setText("Alarm canceled");
    //  }

}