package com.amastudio.pull_up.Activityes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.amastudio.pull_up.Activityes.DashBoardActivity;
import com.amastudio.pull_up.Adapters.OnBoardScreenItem;
import com.amastudio.pull_up.Adapters.OnBoardViewPagerAdapter;
import com.amastudio.pull_up.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class OnBoardActivity extends AppCompatActivity {

    private ViewPager screenPager;
    OnBoardViewPagerAdapter onBoardViewPagerAdapter;

    TabLayout tabIndicator;

    Button nextButton;
    Button getStartButton;

    TextView skip_btn;

    Animation getStartButtonAnim;

    int position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboard);

        // when this activity is about to be launch check if opened before or not

        if (restorePrefData()) {
            Intent dashbordActivity = new Intent(getApplicationContext(),
                    DashBoardActivity.class);
            startActivity(dashbordActivity);
            finish();
        }

        // init views

        tabIndicator = findViewById(R.id.tab_indicator);
        nextButton = findViewById(R.id.next_btn_on_board);
        skip_btn = findViewById(R.id.skip_text_view_on_board);
        getStartButton = findViewById(R.id.get_start_btn_on_board);
        getStartButtonAnim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.get_start_button_animation);

        // strings in title

        String title_one =  getResources().getString(R.string.title_one);
        String title_two =  getResources().getString(R.string.title_two);
        String title_three =  getResources().getString(R.string.title_three);
        String title_four =  getResources().getString(R.string.title_four);
        String title_five =  getResources().getString(R.string.title_five);
        String title_six =  getResources().getString(R.string.title_six);

        // strings in description

        String description_one =  getResources().getString(R.string.description_one);
        String description_two =  getResources().getString(R.string.description_two);
        String description_three =  getResources().getString(R.string.description_three);
        String description_four =  getResources().getString(R.string.description_four);
        String description_five =  getResources().getString(R.string.description_five);
        String description_six =  getResources().getString(R.string.description_six);


        // fill list screen

        final List<OnBoardScreenItem> mList = new ArrayList<>();
        mList.add(new OnBoardScreenItem(title_one, description_one, R.drawable.ic_on_board_one));
        mList.add(new OnBoardScreenItem(title_two, description_two, R.drawable.ic_on_board_two));
        mList.add(new OnBoardScreenItem(title_three, description_three, R.drawable.ic_on_board_three));
        mList.add(new OnBoardScreenItem(title_four, description_four, R.drawable.ic_on_board_four));
        mList.add(new OnBoardScreenItem(title_five, description_five, R.drawable.ic_on_board_five));
        mList.add(new OnBoardScreenItem(title_six, description_six, R.drawable.ic_on_board_six));

        // setup viewPager

        screenPager = findViewById(R.id.screen_viewpager);
        onBoardViewPagerAdapter = new OnBoardViewPagerAdapter(this, mList);
        screenPager.setAdapter(onBoardViewPagerAdapter);

        // setup tabLayout indicator in viewPager

        tabIndicator.setupWithViewPager(screenPager);

        // next button setClick

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = screenPager.getCurrentItem();
                if (position < mList.size()) {
                    position++;
                    screenPager.setCurrentItem(position);
                }

                if (position == mList.size() - 1) { // when to the last screen


                    loadLastScreen();
                }
            }
        });

        // get start button setClick

        getStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open DashBoard Activity

                Intent dashBoardActivity = new Intent(getApplicationContext(),
                        DashBoardActivity.class);
                startActivity(dashBoardActivity);

                // check boolean in new User true and false in SharedPreference save

                savePrefData();
                finish();
            }
        });

        // skip button click listener

        skip_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenPager.setCurrentItem(mList.size());
            }
        });

        // tabLayout add change listener

        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == mList.size() - 1) {

                    loadLastScreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private boolean restorePrefData() {
        SharedPreferences newUserPref = getApplicationContext().getSharedPreferences(
                "newUser", MODE_PRIVATE);
        Boolean OnboardOpenedBefore = newUserPref.getBoolean("isOnBoardOpened", false);
        return OnboardOpenedBefore;
    }

    private void savePrefData() {

        SharedPreferences newUserPref = getApplicationContext().getSharedPreferences(
                "newUser", MODE_PRIVATE);
        SharedPreferences.Editor editor = newUserPref.edit();
        editor.putBoolean("isOnBoardOpened", true);
        editor.commit();
    }


    // show getStartedButton to the last screen

    private void loadLastScreen() {
        getStartButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);
        skip_btn.setVisibility(View.INVISIBLE);

        // getStartButton animation

        getStartButton.setAnimation(getStartButtonAnim);
    }
}
